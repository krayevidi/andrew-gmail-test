import json
import re

import requests

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from backend.gmail.models import Message
from backend.api.gmail.serializer import MessageSerializer


@api_view(['get'])
def message_list(request):
    social_account = request.user.social_auth.first()
    access_token = social_account.extra_data['access_token']
    api_url_base = 'https://www.googleapis.com'
    messages_url_path = '/gmail/v1/users/{}/messages/'.format(
        social_account.uid)
    list_url = '{}{}'.format(api_url_base, messages_url_path)
    batch_url = '{}/batch/'.format(api_url_base)
    headers = {
        'Authorization': 'Bearer {}'.format(access_token)
    }
    list_params = {
        'maxResults': 100
    }

    list_resp = requests.get(list_url, headers=headers, params=list_params)

    if list_resp.status_code == 200:
        message_id_list = [m['id'] for m in list_resp.json()['messages']]
        messages_qs = Message.objects.filter(gmail_id__in=message_id_list)
        fetched_id_list = messages_qs.values_list('gmail_id', flat=True)
        not_fetched_id_list = list(set(message_id_list) - set(fetched_id_list))
        new_messages = []
        batch_body_list = []

        if not_fetched_id_list:
            for message_id in not_fetched_id_list:
                batch_body_list.append(
                    "--BOUNDARY\n"
                    "Content-Type: application/http\n\n"
                    "GET {path}{id}?format=minimal\n".format(
                        path=messages_url_path, id=message_id
                    )
                )
            batch_body_list.append("--BOUNDARY--\n")
            batch_body = '\n'.join(batch_body_list)
            headers['Content-Type'] = 'multipart/mixed; boundary=BOUNDARY'
            batch_resp = requests.post(batch_url, batch_body, headers=headers)

            if batch_resp.status_code == 200:
                resp_body = batch_resp.content.decode('utf8')
                messages = re.findall(r'\{\n.*?\}\n', resp_body, re.DOTALL)

                for message in messages:
                    message = json.loads(message)
                    new_messages.append(Message(
                        gmail_id=message['id'],
                        snippet=message['snippet'],
                    ))
                Message.objects.bulk_create(new_messages)
            else:
                return Response({'details': 'Not implemented.'},
                                status=status.HTTP_501_NOT_IMPLEMENTED)

        messages_list = new_messages + list(messages_qs)
        serializer = MessageSerializer(instance=messages_list, many=True)
        response_data = {
            'messages': serializer.data
        }
        return Response(response_data)
    return Response({'details': 'Not implemented.'},
                    status=status.HTTP_501_NOT_IMPLEMENTED)
