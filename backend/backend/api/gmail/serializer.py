from rest_framework import serializers

from backend.gmail.models import Message


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = [
            'gmail_id',
            'snippet'
        ]
