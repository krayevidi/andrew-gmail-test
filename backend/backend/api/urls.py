from django.conf.urls import url

from backend.api.gmail.views import message_list

urlpatterns = [
    url(r'^messages/$', message_list),
]
