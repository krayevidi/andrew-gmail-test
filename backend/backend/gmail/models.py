from django.db import models


class Message(models.Model):
    gmail_id = models.CharField(max_length=16, unique=True)
    snippet = models.CharField(max_length=255)

    def __str__(self):
        return str(self.gmail_id)
