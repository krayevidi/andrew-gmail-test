from django.contrib import admin

from backend.gmail.models import Message


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = (
        'gmail_id',
        'snippet'
    )
