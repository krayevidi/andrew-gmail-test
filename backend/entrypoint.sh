#!/bin/bash

if [ ! -f backend/settings/local.py ]; then
    cp backend/settings/local.example.py backend/settings/local.py
    chown 1000:1000 backend/settings/local.py
fi

python manage.py runserver 0.0.0.0:8000