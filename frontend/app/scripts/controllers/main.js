'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('MainCtrl', function ($rootScope, $auth) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    $rootScope.$auth = $auth;

    $rootScope.login = function () {
      $auth.authenticate('google', {provider: 'google-oauth2'}).then(function (response) {
        $rootScope.user = response.data;
      });
    };
    $rootScope.logout = function () {
      if (confirm('Are you sure you want to logout?')) {
        delete $rootScope.user;
        $auth.logout();
      }
    }
  });
