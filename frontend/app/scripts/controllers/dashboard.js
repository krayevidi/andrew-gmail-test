'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:DashboardCtrl
 * @description
 * # DashboardCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('DashboardCtrl', function ($scope, $http, API_ROOT) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    $scope.loading = true;
    $http.get(API_ROOT + 'messages/').then(function (response) {
      $scope.loading = false;
      $scope.messages = response.data.messages;
    }, function (response) {
      $scope.loading = false;
      alert('Whoops! Something went wrong.')
    })
  });
