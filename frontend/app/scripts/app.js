'use strict';

/**
 * @ngdoc overview
 * @name frontendApp
 * @description
 * # frontendApp
 *
 * Main module of the application.
 */
angular
  .module('frontendApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'satellizer',
    'route-segment',
    'view-segment'
  ])
  .constant('API_ROOT', '/api/')
  .config(function ($locationProvider, $routeSegmentProvider, $authProvider, API_ROOT) {
    $locationProvider.html5Mode(true);

    $authProvider.google({
      clientId: '',
      url: API_ROOT + 'login/social/jwt_user/',
      scope: [
        'email',
        'https://www.googleapis.com/auth/gmail.readonly'
      ],
      redirectUri: window.location.origin + '/'
    });
    $authProvider.tokenType = 'JWT';

    $routeSegmentProvider.when('/', 'main.dashboard')
      .segment('main', {
        templateUrl: 'views/main.html'
      })
      .within()
      .segment('dashboard', {
        templateUrl: 'views/dashboard.html'
      })
  });