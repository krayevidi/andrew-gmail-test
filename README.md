# Andrew Test

##### Requirements

* docker-compose

#### Bootstrap

```sh
$ make 
```

#### Project requirements

###### set Google creds
for backend:
`backend/settings/local.py` or `base.py`
```python
SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = "{your key}"
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = "{your secret}"
```
for frontend:
`frontend/app/scripts/app.js`
```js
$authProvider.google({
      clientId: '{your key}',
      ...
})
```

#### Urls
`http://localhost:8080/` - frontend

`http://localhost:8080/admin/` - admin