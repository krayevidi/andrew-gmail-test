default: setup

setup:
	docker-compose up -d
	docker-compose exec backend python manage.py collectstatic --noinput
	docker-compose exec backend python manage.py migrate --noinput
	docker-compose exec backend python manage.py createsuperuser
